# argus-interactor: Use Case Architectural Layer

## Description

This *-interactor project contains artifacts supporting use cases, aka interactors. It exists to separate use case concerns and support independent developability and deployment.

## License
See the peer document LICENSE

## Project status
Started

## Documentation
The single responsibility for this project is to provide the implementation of Use Cases. In the Clean Architecture onion diagram, this project corresponds to the Use Case layer.

The Argus Use Cases are:

- Manage profiles (add, remove, select-toggle, select-one, show list, show details, edit)
+ addProfile(ProfileRepo, Profile)
+ removeProfile(ProfileRepo, Profile)
+ selectProfile(Profile)
+ getProfiles(ProfileRepo): List<Profile>
+ getProfile(ProfileId, ProfileRepo): Profile

- Manage networks, aka watch providers (add, show details, edit)
- Manage genres (select-toggle, select-multiple, show list)
- Manage watchlist (add, remove, watch)
- Show Video History List (with filtering for profiles and genres; find by text)
- Show Continue Watching List (with filtering for profiles and genres; find by text)
- Show Video Details (movie, tv show, tv series, tv episode)
- Show Cast List (...)
- Show Crew List (...)
- Show Episode Guest Star List (...)
- Show Cast Details (including filmography)
- Show Crew Details (including filmography)
- Show Popular Videos (fitered by genre)
- Show Top Rated Videos (filtered by genre)
- Search (top level for videos)
