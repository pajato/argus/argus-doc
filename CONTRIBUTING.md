# Contribution Guide (wip):

## General

All code contributions are expected to be clean code.

This means the code generally adheres to the principles and practices espoused by Uncle Bob (Robert Martin) in his
classes, books, blog posts, articles and videos.

For classes, books, blog posts and articles see [cleancoder.com](http://cleancoder.com/products).

For videos see both [cleancoders.com](https://cleancoders.com) and YouTube where you can find many free talks and
presentations.

Clean code for the projects also means the code adheres to a) the
[Kotlin Coding Conventions](https://kotlinlang.org/docs/coding-conventions.html) for Kotlin version 1.9.10, and b)
the [Android Kotlin Style Guide](https://developer.android.com/kotlin/style-guide#structure) with one, important exception:
block constructs ({ ... }), do not have to use Egyptian Brackets ( ... {\n ... } ) when the construct will fit on one line.

## Pull Requests (PR)

For a PR to be accepted (merged) the following will be evaluated and considered:

Is the source code clean? Is it spotless? Does it have a bad smell?
- Are there spurious and unnecessary comments?
- Are functions of length ~4 lines long, preferably less?
- Is block structure employed (see Structure and Interpretation fo Computer Programs - SICP, by Abelson & Sussman, MIT Press)
- Does the source code scream the project name?
- Do names reveal intent?
- Was the code developed using TDD?
- Is the code 100% code covered as measured by Kover?
- Are there no warnings?
- Does the code adhere to the SOLID Single Responsibility Principle (SRP)?
- Is the (SRP) single reason for change identified in classes, objects, interfaces and functions?

Does the PR address a bug, issue, etc. identified and possibly discussed on the relevant GitLab project?

## Todo tasks

See the separate file TODO.md

## Application Startup and Caching (Contributed by Mark Pralat)

Argus has tried data retrieval from TMDb servers with and without caching the information.
The information retrieved is TV, Movie and Person (actors, directors, producers, etc.) information.
It has been observed that during application startup there is approximately a 5 second delay when caching is not used.

According to TMDb and this link: https://developer.themoviedb.org/docs/rate-limiting the legacy rate limits were removed but there is still a rate limit:  "somewhere in the 50 requests per second range. This limit could change at any time so be respectful of the service we have built and respect the 429 if you receive one."

Currently, Argus employs a caching mechanism to store the TV, Movie and Person information so that there is no noticeable delay in application startup.

The caching mechanism is local to the device. The size of the cache is not configurable. Presently the cache size is X MB of information which is comprised 100 movies, 70 TV shows and 200 actors. Future development (paid feature?) will allow offsite storage using Firebase, GCP, Azure or AWS.
