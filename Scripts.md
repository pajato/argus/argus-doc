# Shell scripts

Argus is developed using many (30 and counting) small projects. To facilitate doing operations on multiple projects
quickly and accurately, a handful of shell scripts are provided in the scripts directory.

## Fetching

To fetch all projects from Gitlab use fetch-all.sh which in turn uses fetch-project.sh.

## Building

To build projects cleanly from scratch, assuming projects are cloned into ~/IdeaProjects/, use one of the following:

- build-all.sh to build all the Argus app sub-projects
- build-argus.sh to build just the Argus sub-projects
- build-tks.sh to build the TMDB Kotlin SDK (TKS) sub-projects
- build-project.sh to build a single project

## Git status

To get status on projects, use one of the following:

- git-status-all.sh to get status for all the Argus app sub-projects.
- git-status-argus.sh to get status for just the Argus sub-projects.
- git-status-tks.sh to get status for just the TMDB Kotlin SDK sub-projects.
