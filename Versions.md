# Argus Project Versions

## Current versions

The following dependency tables represent the current version pushed to Gitlab along with the dependent libraries.

## Dependencies

See the file master.libs.versions.toml for the latest version information.

The following table represents the current version dependency relationships pushed to Gitlab.

| Project                   | Depndencies (project:feature:layer)                        |
|---------------------------+------------------------------------------------------------|
| pcp-kmp-lib               |                                                            |
|---------------------------+------------------------------------------------------------|
| pajato-logger             |                                                            |
| pajato-test               | pajato-logger                                              |
| pajato-i18n-strings       |                                                            |
| pajato-persister          |                                                            |
| pajato-uri-validator      |                                                            |
|---------------------------+------------------------------------------------------------|
| tks-common-core           |                                                            |
| tks-searchPager-core      | tks-common-core                                            |
| tks-episode-core          | tks-common-core                                            |
| tks-movie-core            | tks-common-core                                            |
| tks-multi-core            | tks-common-core                                            |
| tks-network-core          | tks-common-core                                            |
| tks-person-core           | tks-common-core                                            |
| tks-season-core           | tks-[common episode]-core                                  |
| tks-tv-core               | tks-[common episode season]-core                           |
| tks-searchMovie-core      | tks-[common searchPager]-core                              |
| tks-searchMulti-core      | tks-[common searchPager]-core                              |
| tks-searchPerson-core     | tks-[common searchPager]-core                              |
| tks-searchTv-core         | tks-[common searchPager]-core                              |
| tks-common-adapter        | tks-[common episode season tv]-core                        |
| tks-episode-adapter       | tks-[common episode season]-core tks-common-adapter        |
| tks-movie-adapter         | tks-[common movie]-core tks-common-adapter                 |
| tks-network-adapter       | tks-[common network]-core tks-common-adapter               |
| tks-person-adapter        | tks-[common person]-core tks-common-adapter                |
| tks-season-adapter        | tks-[common episode]-[core adapter] tks-season-core        |
| tks-tv-adapter            | tks-[common episode season]-[core adapter] tks-tv-core     |
| tks-searchMovie-adapter   | tks-[common search[Pager Movie]]-core tks-common-adapter   |
| tks-searchMulti-adapter   | tks-[common search[Pager Multi]]-core tks-common-adapter   |
| tks-searchPerson-adapter  | tks-[common search[Pager Person]]-core tks-common-adapter  |
| tks-searchTv-adapter      | tks-[common search[Pager Tv]]-core tks-common-adapter      |
|---------------------------+------------------------------------------------------------|
| argus-doc                 |                                                            |
| argus-shared-core         |                                                            |
| argus-filter-core         | pajato-[i18n-strings persister]                            |
| argus-genre-core          | pajato-[i18n-strings persister] argus-shared-core          |
| argus-network-core        | pajato-[i18n-strings persister] argus-shared-core          |
| argus-profile-core        | pajato-[i18n-strings persister] argus-shared-core          |
| argus-info-core           | pajato-[i18n-strings persister]                            |
|                           | tks:[common episode movie person season tv]:[core adapter] |
| argus-videoShelf-core     | pajato-[i18n-strings persister uri-validator]              |
|                           | argus-info-core                                            |
| argus-filter-uc           | pajato-[i18n-strings persister] argus-filter-core          |
| argus-genre-uc            | pajato-[i18n-strings persister] argus-genre-core           |
| argus-network-uc          | pajato-[i18n-strings persister] argus-network-core         |
| argus-profile-uc          | pajato-[i18n-strings persister] argus-profile-core         |
| argus-info-uc             | pajato-[i18n-strings logger persister uri-validator]       |
|                           | argus-[info network]-core                                  |
|                           | tks-[common episode movie network search season tv]-core   |
|                           | tks-[common episode movie network search season tv]-adapter|
| argus-videoShelf-uc       | pajato-[i18n-strings persister uri-validator]              |
|                           | argus-[info-[core uc] videoShelf-core]                     |
| argus-filter-adapter      | pajato-[i18n-strings persister uri-validator]              |
|                           | argus-filter-core                                          |
| argus-network-adapter     | pajato-[i18n-strings persister uri-validator]              |
|                           | argus-network-core                                         |
| argus-profile-adapter     | pajato-[i18n-strings persister uri-validator]              |
|                           | argus-[profile shared]-core                                |
| argus-info-adapter        | pajato-[i18n-strings persister uri-validator]              |
|                           | argus-info-[core uc]                                       |
|                           | tks-[common episode movie person season tv]-[adapter core] |
| argus-videoShelf-adapter  | pajato-[i18n-strings uri-validator persister]              |
|                           | argus-[info videoShelf]-[core uc] argus-info-adapter       |
|                           | tks-[common episode movie person season tv]-[adapter core] |
| argus-main-android-exp    | pajato-[i18n-strings uri-validator]                        |
|                           | argus-[filter genre info network]-[adapter core uc]        |
|                           | argus-[profile tv videoShelf]-[adapter core uc]            |
|                           | argus-shared-core                                          |
|                           | tks-[common episode movie network person]-[adapter core]   |
|                           | tks-[season search[Movie Multi Tv]-[adapter core]          |
|                           | tks-searchPager-core                                       |
|---------------------------+------------------------------------------------------------|


| Project                     | Dependents (project:feature:layer)                                        |
|-----------------------------|---------------------------------------------------------------------------|
| pcp-kmp-plugin              | *:*:*                                                                     |
| pajato-logger               | pajato-test                                                               |
| pajato-test                 | argus-*-*                                                                 |
| tks-common-core             | tks-[core adapter]-[episode network search tv] argus-main-android-exp     |
| tks-episode-core            | tks-core-[season tv] tks-adapter-[common episode] argus-main-android=exp  |
| tks-network-core            | tks-adapter-[common episode network tv] argus-main-android-exp            |
| tks-core-search             | tks-adapter-search argus-main-android-exp                                 |
| tks-season-core             | tks-core-tv tks-adapter-[common episode season tv] argus-main-android-exp |
| tks-tv-core                 | tks-common-adapter argus-main-android-exp                                 |
| tks-common-adapter          | tks-adapter-[episode tv] argus-main-android-exp                           |
| tks-episode-adapter         | tks-adapter-[common season tv] argus-main-android-exp                     |
| tks-network-adapter         | tks-adapter-tv                                                            |
| tks-adapter-search          | argus-main-android-exp                                                    |
| tks-season-adapter          | tks-adapter-[common episode season] argus-main-android-exp                |
| tks-tv-adapter              | argus-main-android argus-main-android-exp                                 |
| argus-doc                   |                                                                           |
| argus-common-core           | argus-[core adapter]-[info network profile video] argus-common-adapter    |
| argus-network-core          | argus-adapter- [network video] argus-uc-network argus-main-android-exp    |
| argus-profile-core          | argus-adapter- [profile video] argus-uc-profile argus-main-android-exp    |
| argus-videoShelf-core       | argus-adapter-[common video]                                              |
| argus-info-core             | argus-[adapter uc]-info                                                   |
| argus-info-uc               | argus-main-android-exp argus-adapter-video                                |
| argus-network-uc            | argus-main-android-exp                                                    |
| argus-profile-uc            | argus-main-android-exp                                                    |
| argus-videoShelf-uc         | argus-main-android-exp                                                    |
| argus-common-adapter        | argus-adapter-[info network profile video] argus-main-android-exp         |
| argus-info-adapter          | argus-adapter-video argus-main-android-exp                                |
| argus-network-adapter       | argus-main-android-exp                                                    |
| argus-profile-adapter       | argus-main-android-exp                                                    |
| argus-videoShelf-adapter    | argus-main-android-exp                                                    |
| argus-main-android          |                                                                           |
| argus-main-android          |                                                                           |
|-----------------------------|---------------------------------------------------------------------------|

## Shell scripts

See the file Scripts.md for documentation on some shell scripts to support fetching projects, building projects,
getting git status, etc.
