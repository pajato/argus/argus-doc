# argus-doc

Version 0.9.5

This project exists to provide detailed documentation about the [Argus projects](https://gitlab.com/pajato/argus).

This file exists to get you started on the road to understanding, critiquing and contributing to the
Argus projects.

## Streaming Video App Background

The Argus projects exist to provide a set of applications and libraries that help Users manage
their video streaming experience. Before Argus, a User would use the apps provided by Netflix, HBO, Hallmark, PBS,
etc. in order to choose a video to watch, possibly see a history of videos they have already watched, learn about
new videos to stream, and continue to watch a series or re-watch an old favorite, among other purposes.

If a User only uses one or two apps, the streaming video experience is pretty simple. When Users start using a half
dozen or more apps, the experience gets very complicated, very fast. Users find themselves asking questions like:

"Have we watch the movie or tv show X?"

"What network was that show on that we started watching last week, or was it last month?"

"Did we see this episode already?"

"Where did we see that actress before?"

"How do you get rid of this show ... I can't stand it anymore?"

"How does this app pause?"

"Oh my God, the next episode is playing already ... that's not what I want!"

And the list goes on and on the more apps you use.

Argus exists to solve as many of these issues as possible in such a way that the User only has to learn one way of
interacting with video streaming apps, i.e. to provide a normalized video streaming experience.

## The seven Argus applications

At the usage level, Argus provides seven applications and many libraries.

The applications support these platforms:

- Android (phone, tablet, watch, tv)
- iOS (phone, tablet, watch, tv)
- Web
- JVM (Swing)
- Linux (tbd)
- Windows (tbd)
- MacOS (tbd)

### Libraries

To facilitate independent development and deployment, Argus structures code into libraries that the application layers
depend on. Generally a dependency is structured by layer and feature.

The five layers used in Argus are:

- Main
- UI
- Adapter (aka Adapter or Interface Adaptor)
- UC (aka Use Case or Interactor)
- Core (aka Entity or Data)

The features identified so far for Argus are:

- Common (code common to a layer, used by other features in the layer)
- Profile
- Streamer (aka Network or Watch Provider)
- History
- Continue
- Watchlist
- Paused
- Hidden
- Finished
- Genre
- Info
- Onboarding

### Status

The Android phone app is in alpha test. None of the others have been started yet.

## Licensing

All Argus applications and libraries are Free Software (source code is freely available) and published using the
[Gnu General Public License, V3](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Tiers, Pricing and Selling Information

All Argus applications will provide a free (no cost) tier that will run on a single device with User data stored only
on that single device.

The Argus roadmap includes offering a paid tier which will use cloud services to provide features like the ability to
access the same data on any device you own. This cost will be nominal and will not provide profit. The cost will cover
cloud service expenses. It is expected this will be on the order of a few dollars per year for very heavy cloud use and
zero for most casual use.

Also in the roadmap is a feature that will allow, only with the User's express permission, the sale of video watching
information to third parties, with the User getting ~90% of the amount paid and the other ~10% used to cover costs
associated with providing this feature.

There will be no ads on any Argus branded application, ever. Branded versions of Argus will be available with special
licensing for a significant one-time fee.

## Argus Software

The Argus software can be characterized on two levels: Clean Code and Kotlin Multiplatform

### Clean Code

The Argus projects strive to provide clean code as espoused by Uncle Bob's
[clean coder organization](https://cleancoders.com/).

For Argus this means:

- Functions are small, usually 1 to 4 lines long, never more.
- Design and implementation is driven by tests. 100% test coverage is mandatory and enforced at build time.
- Names are meant to reveal intent. Naming is hard so names are frequently changed to better reveal intent.
- Function structure is simple: only a few arguments, adherence to the use of
[CQS](https://en.wikipedia.org/wiki/Command%E2%80%93query_separation#:~:text=Command%2Dquery%20separation%20(CQS),the%20caller%2C%20but%20not%20both.),
use of [Screaming Architecture](https://blog.cleancoder.com/uncle-bob/2011/09/30/Screaming-Architecture.html), etc.
- The [architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) consists of four
layers: Core (Entity), Interactor (Use Cases), Adapter (Interface Adapters), Main (Frameworks & Drivers)
- Independent developability by layers (see more below)
- [SOLID principles](https://blog.cleancoder.com/uncle-bob/2020/10/18/Solid-Relevance.html) are standard
- [Functional programming](https://blog.cleancoder.com/uncle-bob/2018/04/13/FPvsOO.html) style is encouraged and used
heavily, but not exclusively.
- The [Booch Principle](https://www.goodreads.com/quotes/7029841-clean-code-is-simple-and-direct-clean-code-reads-like)
is also heavily used: "Clean code is simple and direct. It reads like well-written prose." See more below.

### Branching Strategy

Argus uses a trunk-only branching strategy to minimize (practically eliminate) merge conflicts. This works because the
Argus sub-projects are small. That said, branches can be used to facility collaboration prior to pushing up a commit.
All commits pushed to the trunk will be processed by GitLab to verify the commit has been reviewed by an owner, the
build is warning free, runs all tests successfully and has 100% code coverage. This achieved, GitLab will merge
the commits to the trunk.

### Kotlin Multiplatform

All Argus source code is written in the Kotlin language and complies with the
[Kotlin Coding Standard guidelines](https://kotlinlang.org/docs/coding-conventions.html).

The [Kotlin Multiplatform](https://kotlinlang.org/docs/multiplatform.html) capability provides the glue that allows
applications and libraries to work across Android, iOS, JavaScript, JVM and native OS code. The gist of Kotlin
Multiplatform is that most code is written using a "common" base and specific target code is written using an escape
mechanism using ```expect``` and ```actual``` keywords. See the
[Jetbrains Kotlin documentation](https://kotlinlang.org/docs/home.html) for all Kotlin details. It is excellent.

### Independent Developability

The reason independent developability is important is to minimize source code control conflict resolution, a form of
software development hell that has existed for decades. It all began the day after two developers started working on a
single code base.

The key to independent developability is to divide an application into many projects where each project is developed by
a very small team. These projects each produce a "deployable" product that is combined into a versioned collection used
to build the single application executable. Think Android APK, Web war file, Windows (or Linux or Mac) native
executable, etc.

This division works well to minimize conflicts across teams but cannot do anything about conflicts generated within a
team. Smaller teams leads to fewer conflicts.

Argus uses one or more (generally one) jar files per layer to provide independent developability.

#### Multiple Projects vs Multiple Modules
From Gemini:

The choice between multiple Gradle modules in a single parent project or multiple independent Gradle projects depends on several factors:

Multiple Gradle modules in a single parent project:

o Advantages:

+ Better organization and management of codebase
+ Shared dependencies and configurations
+ Easier cross-project dependencies
+ Simplified build process

o Disadvantages:

- Can become complex for large projects
- Potential for circular dependencies
- Increased build times

Multiple independent Gradle projects:

o Advantages:

+ More modular and maintainable
+ Independent development and testing
+ Easier to scale and reuse

o Disadvantages:

- More complex project structure
- Increased overhead for dependency management and coordination
- Potential for version conflicts

Here are some guidelines to help you decide:

Project size and complexity:

If your project is relatively small and simple, multiple modules might be sufficient.
However, for larger projects with complex dependencies and multiple teams, independent projects might be more manageable.

Team structure:

If your team is organized into multiple subteams with distinct responsibilities, independent projects can align with their work.

Deployment requirements:

If your modules need to be deployed independently, independent projects are necessary.

Build times:

If build times are a concern, multiple modules can potentially improve performance by leveraging shared dependencies and configurations.
However, for very large projects, independent projects might be faster.

Ultimately, the best approach depends on your specific project requirements and team dynamics.
Consider the factors mentioned above and experiment with different structures to find the one that works best for you.

### Booch Principle

We rarely see code that reads like well written prose. In structure alone, we have been indoctrinated for decades to
write code that has only marginally improved over the lists of binary numbers that characterized Alan Turing's initial
forays into writing programs. We graduated to assembly code, which were lists of statements. No prose yet.

Then we started using "high-order" languages, like Cobol, C, Fortran, Algol.  Easier to read lists of statements. But
still nothing that you see in well written prose. No sentences, paragraphs, chapters, books, etc.

It was only with Donald Knuth's [Literate Programming](https://en.wikipedia.org/wiki/Literate_programming)
that prose like code started to appear.  But Literate Programming landed with a thud, is rarely seen and is difficult
to construct.

Clean Code, leveraging functional programming style, does allow for more prose like code. Lines of code are generally
longer and more sentence like. Functions are paragraph like. Files become chapter like. Module or components can be
considered as collections of chapters. And the overall code base tells a story. Grady Booch sees us software developers
as first and foremost, story tellers. This then, is the story of Argus.

#### Examples
```kotlin
    override suspend fun fetchLines(uri: URI): List<Profile> =
        uri.toURL().readText().split("\n").filter { it.isNotEmpty() }.map { it.loadLine() }

    private fun String.loadLine(): Profile = Json.decodeFromString(TestProfileSerializer, this)

    override suspend fun persist(item: Profile?, uri: URI) { if (item == null) updateAll(uri) else item.update(uri) }
```

### Building Argus Projects

Gradle is used to build all Argus projects. In order to do this safely, Argus does not supply the Gradle wrapper
executables. See the file Gradle.md for details on why and how to get safe versions of the Gradle wrapper executables.

## Getting Started

To get started with Argus, run the script "fetch-all.sh" which will download all the projects, build them all so that
Argus apps can be deployed or run.

See Contributing.md for more details.

## Preparaing to release a build

1. Prepare your release build:
   Ensure you have a release-ready version of your app.
   Build your app for release using Android Studio.
   Select the "Build" menu, then "Build Bundle(s) / APK(s)" and choose "Release".
   Locate the generated release APK file, typically in the app/build/outputs/apk/release/ directory.

2. Connect your device or emulator:
   Connect your Android device or start an Android emulator.
   Make sure the device or emulator is unlocked and ready to receive the APK.

3. Install the APK:
   Transfer the release APK file to your device or emulator.
   Locate the APK file on your device or emulator using a file manager app.
   Tap on the APK file to initiate the installation process.
   Follow the on-screen prompts to grant necessary permissions and complete the installation.

4. Verify the installation:
   Once the installation is complete, you should be able to find the app icon in the app drawer of your device or
   emulator.
   Launch the app to ensure it is functioning correctly.

Additional Notes:
   Make sure you have the necessary permissions to install apps on your device.
   If you encounter any errors during installation, check the logcat output in Android Studio for more details.
   You can also use the adb install command to install the APK from the command line.

## Reporting bugs and requesting features: Issues

Issues (features and bugs) are maintained in this project and are managed using the GitLab web interface. Issues can be
filed using email.

### Using email to file issues

You can create a new Argus issue inside this project by sending an email to the following email address:

incoming+pajato-argus-argus-doc-41676765-ozprmhRStYYWqKAUjeD5-issue@incoming.gitlab.com

The subject will be used as the title of the new issue, and the message will be the description. Quick actions and
styling with Markdown are supported.

* Acknowledgements

There have been many influences in the design and implemnentation of Argus/EXP and Argus/MP

** People
*** Sandy Scott
*** Bryan Scott
*** Paul Matthew Reilly
*** Mark Pralat
*** Brian Gibson
*** Joel
*** Peak
*** Janus Cole
*** John Siracusa
*** Casey Liss
** Articles
*** How to implement Expandable Text in Jetpack Compose
Author: Mun Bonecci
URL: https://medium.com/@munbonecci/how-to-implement-expandable-text-in-jetpack-compose-ca9ba35b645c
** Third Party Libraries
** Software Apps
*** Neatflix
*** Now in Andoid
