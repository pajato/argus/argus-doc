# Interfaces

Argus uses a hierarchical interface model to identify the operations implemented by the repositories used by Argus feature projects.

The most basic is the ValueRepo<Value> which, when implemented, is sufficient to provide in-memory list behavior.

Next is the KeyedRepo<Key, Value> which is sufficient for an in-memory map type behavior.

KeyedFileRepo<Key, Value> adds persisting data to and fetching data from a file.

A SelectRepo is a KeyedFileRepo that provides the ability to select one or more items.

A ToggleRepo is a KeyedFileRepo that provides toggle behavior when selecting an item.
