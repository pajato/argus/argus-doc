# argus-adapter


The [adapter layer](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html), according to Uncle
Bob (aka, The Source) is defined as follows:

    "The software in this layer is a set of adapters that convert data from the format most convenient for the use
    cases and entities, to the format most convenient for some external agency such as the Database or the Web. ..."


For the Argus project, this means that all concrete data repositories, classes and objects are created in this layer
using the interfaces defined in the Entities (core) layer.
