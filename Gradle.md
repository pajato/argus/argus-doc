# Using Gradle Safely

Using Gradle inevitably leads to using the Gradle wrapper.  Mark Murphy, in his
[blog post](https://commonsware.com/blog/2016/09/19/reminder-check-projects-before-importing.html)
discusses why there is a serious security risk in using either of the Gradle wrapper scripts or even the Gradle wrapper
jar file.

In order to mitigate this risk, all Pajato projects do not include the unsafe Gradle wrapper files by default. Instead,
we recommend that contributors safely install a known version of Gradle (preferably using a package manager such as
HomeBrew on a Mac) and exectute the following command in a terminal:

$ gradle wrapper --gradle-version 8.0.2 --distribution-type all

which is lifted drectly from the Gradle manual.
