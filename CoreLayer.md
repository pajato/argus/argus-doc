# CoreLayer

## Description

The innermost architectural layer, corresponds to Uncle Bob's Entity Layer.

## License

GPL, V3, See the peer document LICENSE for details.

## Project status

Started, Current version is 0.9.1

## Documentation

Examples of the main core interfaces are Profile, Network, Video, Cast, Crew, Watchlist, and Genre. These interfaces
scream "video streaming information app".

Other interfaces characterize behaviors:

- Appender - add an single generic item
- Cacheable - access one or all of some generic data
- Fetcher - obtain a generic data item from an external data source
- Finder - obtain a list of items containing particular text
- Injector - inject an implementation of an interface, especially a URI
- Loader - load read-only generic items
- Persister - update one or all items in a repo
- Remover - remove a single generic item
