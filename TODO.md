# Task aka TODO

## General

1. This list is almost always out of date. Check with the current project maintainer for up-to-date information.
2. See the contributions guide (CONTRIBUTION.md) for helpful guidance in generating a fix and/or feature.
3. See the user-stories directory for elaborated task descriptions or to provide an elaborated task description.

## Todo tasks (+ marks currently active task)

- Get Play Store ready
- Android sub-projects (main and ui layers)
  Split out main and re-implement with tests
  Split out each ui feature project and re-implement with tests
- Video abstraction as eiher tv show or movie (possibly a "part")
- Information provider abstraction (TMDb is implemented, Imdb and Omdb are candidates)
- Shelf as abstraction whereby they get registered and displayed
- Profiles selection: one and only one selection model
- Filter items displyed in shelves by Profile, Streaming Provider (aka Network) and Search query
- Onboarding: show workflow examples via screen shots and simple text; fixed pager
- Add Person search shelf
- Prefer No Throws
- Decide on aoproach to use to highlight video names on top of images
  Take 1 is solid background color
  Take 2 is drop shadow ala Netflix
  Take 3 is ?
- Select multiple colors for circular profile icons.
+ Fix info extras bug
- Implement System Prevalence persistence model
- Revisit how to handle episode still paths: cache or not, use Tv or Episode
- Make info shelf sections shelf-aware

## Completed tasks

- Better Dropdown Menus
- Add remove operation to History shelf menu.
- Ratings base code
- Collapsing and expanding filter toolbar for Profiles, Networks and Genres (credit to Heather Scott)
- Conventions plugins (see now in android app!)
- Splash screen
- Watch now
- Add Movie search

## Deferred

- Bottom bar (currently deferred)
- Multi-module destinaton navigation ala raamcosta. Prefer nested normal navigation instead
- Reviews
- TMDb posting
- Normalize TvInfo
- Grid based network xfer
- Grid based profile xfer
- Shelf arranger (drag and drop)
- Extension Shelfs (Popular, For You, etc.)
- Sharing
- Phone Home

Color Palette Hax values
2B4162,385F71,F5F0F6,D7B377
8F754F,3C77D6,784106,26A599
CDE0F1,DD6E42,61E294,985F6F
82D4BB,3F88C5,F9627D,537A5A