  - Refresh redux: make it a task that repeats every 23 hours after
    startup and instantly unpauses appropriate videos
  - KMP/CMP Pajato app/library convention plugins
  - *-ui Project to evaluate Kover on Compose
  - Kotlin 2.0 upgrade
  - argus-main-androic-exp bug fixing
