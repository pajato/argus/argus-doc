Menus should be shelf aware. For example, the Watchlist shelf should remove an item when "Watch Now" is selected; all other
shelves should not do any removing on a "Watch Now" selection.
