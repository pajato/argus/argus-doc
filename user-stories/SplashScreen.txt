Use the old (original Argus icon) as a starting point and then animaate in 100ms increments to a new (modern Argus icon).
Each increment will show a different (still) image from existing or canned videos.
