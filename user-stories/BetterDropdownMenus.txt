See Philipp Lackner's video on dropdown menus (https://www.youtube.com/watch?v=QCSJfMqQY9A). This approach is better than my
initial code in that he controls the menu origin position.

Also, "mark watched" is better than "watch now" and both are perfect.

In a nutshell, here's how menu items should be distributed, by shelf label:

o Continue Watching
- Watch Now
- Mark Watched

o Watchlist
- Watch Now
- Mark Watched
- Edit Video
- Remove Video

o Watched Videos
- Watch Now
- Mark Watched
- Edit Video
- Remove Video
- Transfer Profile
- Transfer Network
- Pause
- Finish
- Hide

o Paused
- Restore
- Finish
- Hide
- Edit Video
- Remove Video

o Finished
- Restore
- Pause
- Hide
- Edit Video
- Remove Video

o Hidden
- Restore
- Pause
- Finish
- Edit Video
- Remove Video
